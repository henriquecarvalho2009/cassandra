package tech.mastertech.itau.cassproj.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cassproj.dtos.Pin;
import tech.mastertech.itau.cassproj.models.PinCategoria;
import tech.mastertech.itau.cassproj.models.PinFavorito;
import tech.mastertech.itau.cassproj.models.PinUsuario;
import tech.mastertech.itau.cassproj.repositories.PinCategoriaRepository;
import tech.mastertech.itau.cassproj.repositories.PinFavoritoRepository;
import tech.mastertech.itau.cassproj.repositories.PinUsuarioRepository;

@Service
public class PinService {
	
	@Autowired
	private PinCategoriaRepository pinCategoriaRepository;
	
	@Autowired
	private PinUsuarioRepository pinUsuarioRepository;
	
	@Autowired
	private PinFavoritoRepository pinFavoritoRepository;
	
	public void salvarPin(Pin pin) {
		PinUsuario pinUsuario = new PinUsuario();
		pinUsuario.setUsuario(pin.getUsuario());
		pinUsuario.setImagem(pin.getImagem());
		pinUsuario.setDescricao(pin.getDescricao());
		pinUsuario.setCategoria(pin.getCategoria());	
		pinUsuarioRepository.save(pinUsuario);
		
		PinCategoria pinCategoria = new PinCategoria();
		pinCategoria.setUsuario(pin.getUsuario());
		pinCategoria.setImagem(pin.getImagem());
		pinCategoria.setDescricao(pin.getDescricao());
		pinCategoria.setCategoria(pin.getCategoria());
		pinCategoriaRepository.save(pinCategoria);
	}
	
	public Iterable<PinCategoria> buscarPinsPorCategoria (String categoria) {
		return pinCategoriaRepository.findByCategoria(categoria);
	}
	
	public Iterable<PinUsuario> buscarPinsPorUsuario(String usuario) {
		return pinUsuarioRepository.findByUsuario(usuario);
	}
	
	public Iterable<PinFavorito> buscarPinsFavoritos(String usuario) {
		return pinFavoritoRepository.findByUsuario(usuario);
	}
	
	public void favoritarPin(PinFavorito pin) {
		pinFavoritoRepository.save(pin);
	}
}
