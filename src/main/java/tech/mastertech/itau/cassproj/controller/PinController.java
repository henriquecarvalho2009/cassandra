package tech.mastertech.itau.cassproj.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cassproj.dtos.Pin;
import tech.mastertech.itau.cassproj.models.PinCategoria;
import tech.mastertech.itau.cassproj.models.PinFavorito;
import tech.mastertech.itau.cassproj.models.PinUsuario;
import tech.mastertech.itau.cassproj.services.PinService;
import tech.mastertech.itau.cassproj.services.UsuarioService;

@RestController
@RequestMapping("/pin")
public class PinController {

	@Autowired
	private PinService pinService;

	@Autowired
	private UsuarioService UsuarioService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void salvarPin(@RequestBody Pin pin, Principal principal) {
		if (!principal.getName().equals(pin.getUsuario())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
					"Os Pins devem ser cadastrados para o usuário logado");
		}
		pinService.salvarPin(pin);
	}
	
	@PostMapping("/favorito")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void favoritarPin(@RequestBody PinFavorito pinFavorito) {
		pinService.favoritarPin(pinFavorito);
	}

	@GetMapping("/{categoria}")
	public Iterable<PinCategoria> buscarPinsPorCategoria(@PathVariable String categoria) {
		return pinService.buscarPinsPorCategoria(categoria);
	}
	
	@GetMapping("/{usuario}")
	public Iterable<PinUsuario> buscarPinsPorUsuario(@PathVariable String usuario) {
		return pinService.buscarPinsPorUsuario(usuario);
	}
	
	@GetMapping("/favoritos/{usuario}")
	public Iterable<PinFavorito> buscarPinsFavoritos(@PathVariable String usuario) {
		return pinService.buscarPinsFavoritos(usuario);
	}

}
