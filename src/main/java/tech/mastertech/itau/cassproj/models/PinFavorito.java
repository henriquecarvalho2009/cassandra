package tech.mastertech.itau.cassproj.models;

import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import com.datastax.driver.core.utils.UUIDs;

@Table
public class PinFavorito {
	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private String usuario;
	private String imagem;
	private String descricao;
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private UUID dataPost = UUIDs.timeBased();
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private String postador;
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private UUID dataFav = UUIDs.timeBased();
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public UUID getDataPost() {
		return dataPost;
	}
	public void setDataPost(UUID dataPost) {
		this.dataPost = dataPost;
	}
	public String getPostador() {
		return postador;
	}
	public void setPostador(String postador) {
		this.postador = postador;
	}
	public UUID getDataFav() {
		return dataFav;
	}
	public void setDataFav(UUID dataFav) {
		this.dataFav = dataFav;
	}
	
	
}
