package tech.mastertech.itau.cassproj.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cassproj.dtos.Usuario;
import tech.mastertech.itau.cassproj.models.Login;
import tech.mastertech.itau.cassproj.models.Perfil;
import tech.mastertech.itau.cassproj.repositories.LoginRepository;
import tech.mastertech.itau.cassproj.repositories.PerfilRepository;
import tech.mastertech.itau.cassproj.security.JwtTokenProvider;

@Service
public class UsuarioService {
	
	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	private PerfilRepository perfilRepository;
	
	@Autowired
	private JwtTokenProvider tokenProvider;
	
	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	public Optional<String> fazerLogin(Login credenciais) {
		Optional<Login> loginOptional = loginRepository.findByUsuario(credenciais.getUsuario());
		
		if (loginOptional.isPresent()) {
			if (encoder.matches(credenciais.getSenha(), loginOptional.get().getSenha())) {
				String token = tokenProvider.criarToken(credenciais.getUsuario());
				return Optional.of(token);
			}
		}
		return Optional.empty();
	}
	
	public void cadastrar(Usuario usuario) {
		cadastrarLogin(usuario);
		cadastrarPerfil(usuario);
		
	}
	
	private void cadastrarLogin(Usuario usuario) {
		Login login = new Login();
		String hash = encoder.encode(usuario.getSenha());
		login.setUsuario(usuario.getUsuario());
		login.setSenha(hash);
		
		loginRepository.save(login);
	}
	
	private void cadastrarPerfil(Usuario usuario) {
		Perfil perfil = new Perfil();
		perfil.setNome(usuario.getNome());
		perfil.setUsuario(usuario.getUsuario());
		perfil.setFotoPerfil(usuario.getFotoPerfil());
		perfil.setBio(usuario.getBio());
		
		perfilRepository.save(perfil);
	}
	
	

}
