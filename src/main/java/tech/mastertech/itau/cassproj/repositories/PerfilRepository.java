package tech.mastertech.itau.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cassproj.models.Perfil;

public interface PerfilRepository extends CrudRepository<Perfil, String>{

}
