package tech.mastertech.itau.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cassproj.models.PinUsuario;

public interface PinUsuarioRepository extends CrudRepository<PinUsuario, String> {
	public Iterable<PinUsuario> findByUsuario (String categoria);
}
