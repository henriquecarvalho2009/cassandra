package tech.mastertech.itau.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cassproj.models.PinFavorito;

public interface PinFavoritoRepository extends CrudRepository<PinFavorito, String> {
	public Iterable<PinFavorito> findByUsuario (String categoria);
}
