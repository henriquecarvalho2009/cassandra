package tech.mastertech.itau.cassproj.models;

import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import com.datastax.driver.core.utils.UUIDs;

@Table
public class PinUsuario {
	private String imagem;
	private String descricao;
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private UUID dataPost = UUIDs.timeBased();
	private String categoria;
	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private String usuario;
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public UUID getDataPost() {
		return dataPost;
	}
	public void setDataPost(UUID dataPost) {
		this.dataPost = dataPost;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
}
