package tech.mastertech.itau.cassproj.dtos;

public class LoginResposta {
	
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
